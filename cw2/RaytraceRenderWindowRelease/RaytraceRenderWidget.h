//////////////////////////////////////////////////////////////////////
//
//	University of Leeds
//	COMP 5812M Foundations of Modelling & Rendering
//	User Interface for Coursework
//
//	September, 2020
//
//  -----------------------------
//  Raytrace Render Widget
//  -----------------------------
//
//	Provides a widget that displays a fixed image
//	Assumes that the image will be edited (somehow) when Render() is called
//  
////////////////////////////////////////////////////////////////////////

// include guard
#ifndef _RAYTRACE_RENDER_WIDGET_H
#define _RAYTRACE_RENDER_WIDGET_H

// include the relevant QT headers
#include <QOpenGLWidget>
#include <QMouseEvent>

// and include all of our own headers that we need
#include "TexturedObject.h"
#include "RenderParameters.h"

// struct for a single lightsource
struct Lightsource {
	Homogeneous4 pos;
	float ambient[4];
	float diffuse[4];
	float specular[4];
};

// class for a render widget with arcball linked to an external arcball widget
class RaytraceRenderWidget : public QOpenGLWidget										
	{ // class RaytraceRenderWidget
	Q_OBJECT
	private:	
	// the geometric object to be rendered
	TexturedObject *texturedObject;	

	// the render parameters to use
	RenderParameters *renderParameters;

	// An image to use as a framebuffer
	RGBAImage frameBuffer;

	// clipping planes
	
	float rightClip = 1.f;
	float leftClip = -1.f;
	float topClip =  1.f;
	float botClip = -1.f;
	float nearClip = 0;
	float farClip =  2.f;
	
	// modelview matrix
	Matrix4 mvMatrix;
	// vector of transformed vertices
	std::vector<Cartesian3> vertices;
	// vector of transformed normals
	std::vector<Cartesian3> normals;


	// default ambient / diffuse / specular colorr
    float surfaceColor[4] = { 0.7, 0.7, 0.7, 1.0 };

	// emissive glow from object
    float emissiveColor[4];
    // object material properties
    float ambientColor[4];
    float diffuseColor[4];
    float specularColor[4];
	// specular shininess
    float shininess; 

	// multi-material vector
	std::vector<std::vector<float>> materials_emissive;
	std::vector<std::vector<float>> materials_ambients;
	std::vector<std::vector<float>> materials_diffuse;
	std::vector<std::vector<float>> materials_specular;
	std::vector<float> materials_shininess;
	 

	float albedoThreshold = 0.001f;

	// vector of light sources
	std::vector<Lightsource> lights;

	RGBAValue ClearColor = RGBAValue(0.8f * 255.f, 0.8f * 255.f, 0.6f * 255.f, 255.f);

	public:
	// constructor
	RaytraceRenderWidget
			(
	 		// the geometric object to show
			TexturedObject 		*newTexturedObject,
			// the render parameters to use
			RenderParameters 	*newRenderParameters,
			// parent widget in visual hierarchy
			QWidget 			*parent
			);
	
	// destructor
	~RaytraceRenderWidget();
			
	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();
	
	/// setup routines 
	void TransformVertices();
	void PrepareLights();
	void PrepareMaterials();
	void LoadMultiMaterials();

	// sees if there is a collision along the specified ray with the given triangle from the textured object
	Homogeneous4 RayCollision(Cartesian3 rayStart, Cartesian3 rayEnd, int faceID, int vmod = 0);

    // routine that generates the image
    void Raytrace();

	// helper stuff
	float BRDF(Cartesian3 normal, Cartesian3 inDir, Cartesian3 outDir);

	float pathTrace(Cartesian3 ray, float combinedAlbedo);

	float indirectLight(Cartesian3 point, Cartesian3 outDir, float combinedAlbedo);

	// generate a MonteCarlo vector with a hemisphere based around the normal
	Cartesian3 MonteCarloHemiVec(Cartesian3 norm);

	// clear
	void ClearBuffer();

	// mouse-handling
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);

	// these signals are needed to support shared arcball control
	public:
	signals:
	// these are general purpose signals, which scale the drag to 
	// the notional unit sphere and pass it to the controller for handling
	void BeginScaledDrag(int whichButton, float x, float y);
	// note that Continue & End assume the button has already been set
	void ContinueScaledDrag(float x, float y);
	void EndScaledDrag(float x, float y);
	}; // class RaytraceRenderWidget

#endif
