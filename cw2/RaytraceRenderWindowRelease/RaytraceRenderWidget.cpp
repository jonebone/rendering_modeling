//////////////////////////////////////////////////////////////////////
//
//  University of Leeds
//  COMP 5812M Foundations of Modelling & Rendering
//  User Interface for Coursework
//
//  September, 2020
//
//  -----------------------------
//  Raytrace Render Widget
//  -----------------------------
//
//	Provides a widget that displays a fixed image
//	Assumes that the image will be edited (somehow) when Render() is called
//  
////////////////////////////////////////////////////////////////////////

#include <math.h>

// include the header file
#include "RaytraceRenderWidget.h"

// constructor
RaytraceRenderWidget::RaytraceRenderWidget
        (   
        // the geometric object to show
        TexturedObject      *newTexturedObject,
        // the render parameters to use
        RenderParameters    *newRenderParameters,
        // parent widget in visual hierarchy
        QWidget             *parent
        )
    // the : indicates variable instantiation rather than arbitrary code
    // it is considered good style to use it where possible
    : 
    // start by calling inherited constructor with parent widget's pointer
    QOpenGLWidget(parent),
    // then store the pointers that were passed in
    texturedObject(newTexturedObject),
    renderParameters(newRenderParameters)
    { // constructor
        LoadMultiMaterials();

} // constructor    

// destructor
RaytraceRenderWidget::~RaytraceRenderWidget()
    { // destructor
    // empty (for now)
    // all of our pointers are to data owned by another class
    // so we have no responsibility for destruction
    // and OpenGL cleanup is taken care of by Qt
    } // destructor                                                                 

// called when OpenGL context is set up
void RaytraceRenderWidget::initializeGL()
    { // RaytraceRenderWidget::initializeGL()
	// this should remain empty
    } // RaytraceRenderWidget::initializeGL()

// called every time the widget is resized
void RaytraceRenderWidget::resizeGL(int w, int h) { // RaytraceRenderWidget::resizeGL()
    // resize the render image
    frameBuffer.Resize(w, h);

    // compute the aspect ratio of the widget
    float aspectRatio = (float) w / (float) h;
    
    // we want to capture a sphere of radius 1.0 without distortion
    // so we set the ortho projection based on whether the window is portrait (> 1.0) or landscape
    // portrait ratio is wider, so make bottom & top -1.0 & 1.0
    if (aspectRatio > 1.0) {
        //glOrtho(-aspectRatio, aspectRatio, -1.0, 1.0, 0, 2.0);
        leftClip = -aspectRatio;
        rightClip = aspectRatio;
        botClip = -1.0f;
        topClip =  1.0f;
    }
        
    // otherwise, make left & right -1.0 & 1.0
    else {
        //glOrtho(-1.0, 1.0, -1.0/aspectRatio, 1.0/aspectRatio, 0, 2.0);
        leftClip = -1.0f;
        rightClip = 1.0f;
        botClip = -1.0f/aspectRatio;
        topClip =  1.0f/aspectRatio;
    }
        

} // RaytraceRenderWidget::resizeGL()
    
// called every time the widget needs painting
void RaytraceRenderWidget::paintGL() { // RaytraceRenderWidget::paintGL()
    // set background colour to white
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    //ClearBuffer();
    Raytrace();

    // and display the image
    glDrawPixels(frameBuffer.width, frameBuffer.height, GL_RGBA, GL_UNSIGNED_BYTE, frameBuffer.block);
} // RaytraceRenderWidget::paintGL()


void RaytraceRenderWidget::TransformVertices() { // RaytraceRenderWidget::TransformVertices()
    vertices.clear();
    normals.clear();

    //Matrix4 mvMatrix = Matrix4();
    mvMatrix.SetIdentity();
    Matrix4 temp = Matrix4();

    // scene translation
    temp.SetTranslation(Cartesian3(renderParameters->xTranslate, renderParameters->yTranslate, -1.f));
    mvMatrix = mvMatrix * temp;

    // scene rotation
    mvMatrix = mvMatrix * renderParameters->rotationMatrix;

    // taken from Render() in TexturedObject
    // Scale defaults to the zoom setting
    float scale = renderParameters->zoomScale;

    if (renderParameters->useBoxMode) scale *= 1.5f;
    
    // if object scaling is requested, apply it as well 
    if (renderParameters->scaleObject)
        scale /= texturedObject->objectSize;
    
    // model translation to Center of Gravity
    Cartesian3 CoG = Cartesian3(texturedObject->centreOfGravity) * -1.f * scale;
    temp.SetTranslation(CoG);
    if (renderParameters->centreObject)
        mvMatrix = mvMatrix * temp;

    for (int i = 0; i < texturedObject->vertices.size(); i ++)
        vertices.push_back((mvMatrix * Homogeneous4(texturedObject->vertices[i] * scale) ).Point());

    mvMatrix.SetIdentity();
    // need to exclude translations when doing normals
    for (int i = 0; i < texturedObject->normals.size(); i ++)
        normals.push_back(((mvMatrix * renderParameters->rotationMatrix) * Homogeneous4(texturedObject->normals[i]) * scale).Vector());

} // RaytraceRenderWidget::TransformVertices()

void RaytraceRenderWidget::PrepareLights() { // RaytraceRenderWidget::PrepareLights()
    lights.clear();

    //Matrix4 mvMatrix = Matrix4();
    mvMatrix.SetIdentity();

    //mvMatrix = mvMatrix * renderParameters->lightMatrix;

    // add default light
    Lightsource defaultLight;
    Homogeneous4 defaultPosition = Homogeneous4(renderParameters->lightPosition[0], renderParameters->lightPosition[1], -1.f * renderParameters->lightPosition[2], renderParameters->lightPosition[3]);
    if (renderParameters->useBoxMode) defaultPosition.w = 0.f;
    
    defaultLight.pos = (mvMatrix * renderParameters->lightMatrix ) *  defaultPosition;


    for (int i = 0; i < 4; i ++)
        defaultLight.ambient[i] = defaultLight.diffuse[i] = defaultLight.specular[i] = renderParameters->lightColor[i];
    // they're all the same in the default light
    //std::cout << defaultLight.ambient[0] << " " << defaultLight.ambient[1] << " " << defaultLight.ambient[2] << " " << defaultLight.ambient[3] << " \n";
    //std::cout << defaultLight.diffuse[0] << " " << defaultLight.diffuse[1] << " " << defaultLight.diffuse[2] << " " << defaultLight.diffuse[3] << " \n";
    //std::cout << defaultLight.specular[0] << " " << defaultLight.specular[1] << " " << defaultLight.specular[2] << " " << defaultLight.specular[3] << " \n";


    lights.push_back(defaultLight);
    
    if (renderParameters->useBoxMode) {
        Lightsource secondaryLight;
        Homogeneous4 secondPos = Homogeneous4(-2., 5., -8., 0.f);
        secondaryLight.pos = secondPos;
        for (int i = 0; i < 4; i ++)
            secondaryLight.ambient[i] = secondaryLight.diffuse[i] = secondaryLight.specular[i] = ((float) i )/ 4.0f;
        lights.push_back(secondaryLight);
    }



} // RaytraceRenderWidget::PrepareLights()

void RaytraceRenderWidget::PrepareMaterials() { // RaytraceRenderWidget::PrepareMaterials()
   
    // copy the intensity into RGB channels
    emissiveColor[0]   = emissiveColor[1] = emissiveColor[2] = renderParameters->emissive;
    emissiveColor[3]   = 1.0; // don't forget alpha
    
    ambientColor[0]        = ambientColor[1]      = ambientColor[2]      = renderParameters->ambient;
    ambientColor[3]        = 1.0; // alpha

    diffuseColor[0]        = diffuseColor[1]      = diffuseColor[2]      = renderParameters->diffuse;
    diffuseColor[3]        = 1.0; // alpha

    specularColor[0]        = specularColor[1]      = specularColor[2]      = renderParameters->specular;
    specularColor[3]        = 1.0; // alpha

    // set the shininess from the specular exponent
    shininess      = renderParameters->specularExponent;



} // RaytraceRenderWidget::PrepareMaterials()

void RaytraceRenderWidget::LoadMultiMaterials() { // RaytraceRenderWidget::LoadMultiMaterials()
    std::vector<float> EM, AM, DM, SM;

    materials_emissive.clear();
    materials_ambients.clear();
    materials_diffuse.clear();
    materials_specular.clear();
    materials_shininess.clear();

    // no emission for most of these
    EM = std::vector<float>(4, 0.f);

    // found at http://www.it.hiof.no/~borres/j3d/explain/light/p-materials.html
    // red plastic
    materials_emissive.push_back(EM);
    materials_ambients.push_back(std::vector<float>{0.f, 0.f, 0.f, 1.f});
    materials_diffuse.push_back(std::vector<float>{0.5f, 0.f, 0.f, 1.f});
    materials_specular.push_back(std::vector<float>{0.7f, 0.6f, 0.6f, 1.f});
    materials_shininess.push_back(32.f);
    // blue plastic
    materials_emissive.push_back(EM);
    materials_ambients.push_back(std::vector<float>{0.f, 0.1f, 0.06f, 1.f});
    materials_diffuse.push_back(std::vector<float>{0.0f,0.50980392f,0.50980392f,1.0f});
    materials_specular.push_back(std::vector<float>{0.50196078f,0.50196078f,0.50196078f,1.0f});
    materials_shininess.push_back(32.f);
    // white plastic
    materials_emissive.push_back(EM);
    materials_ambients.push_back(std::vector<float>{0.f, 0.f, 0.f, 1.f});
    materials_diffuse.push_back(std::vector<float>{0.55f, 0.55f, 0.55f, 1.f});
    materials_specular.push_back(std::vector<float>{0.7f, 0.7f, 0.7f, 1.f});
    materials_shininess.push_back(32.f);
    // polished copper
    materials_emissive.push_back(EM);
    materials_ambients.push_back(std::vector<float>{0.2295f, 0.08825f, 0.0275f, 1.0f });
    materials_diffuse.push_back(std::vector<float>{0.5508f, 0.2118f, 0.066f, 1.0f});
    materials_specular.push_back(std::vector<float>{0.580594f, 0.223257f, 0.0695701f, 1.0f});
    materials_shininess.push_back(51.2f);



} // RaytraceRenderWidget::LoadMultiMaterials()


Homogeneous4 RaytraceRenderWidget::RayCollision(Cartesian3 rayStart, Cartesian3 rayEnd, int faceID, int vmod) {
    // returns Homogeneous4 only bc we want 4 values out of this-
        // three barycentric coordinates, stored to X Y Z
        // indicator of depth on ray, stored to W
    // use vmod to recursively call self when the face has more than 3 vertices

    Cartesian3 faceVertex_0, faceVertex_1, faceVertex_2;
    faceVertex_0 = vertices[texturedObject->faceVertices[faceID][0]]; // v0 will always be root of triangles since obj faces are triangle fans
    faceVertex_1 = vertices[texturedObject->faceVertices[faceID][1 + vmod]];
    faceVertex_2 = vertices[texturedObject->faceVertices[faceID][2 + vmod]];

    

    Cartesian3 rayVec = rayEnd - rayStart;
    Cartesian3 edge01Vec = faceVertex_1 - faceVertex_0;
    Cartesian3 edge02Vec = faceVertex_2 - faceVertex_0;

    // based on the parametric way of checking line-plane collisions found on wikipedia:
        // https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
        // if we can find values t, u, v that satisfy
        // rayStart + rayVec * t = faceVertex_0 + edge01Vec * u + edge02Vec * v     // this can be rearranged a few ways (shown below)
        // then there is an intersection
        // and if  0 <= t <= 1, the intersection is between the start and the end 
        // the collision point is at rayStart + rayVec * t
        // and if u + v <= 1, the intersection is in the triangle defined by faceVertex_0, faceVertex_1, and faceVertex_2
        // the above equation can be rearranged to:
        // rayStart - faceVertex_0  = - rayVec * t + edge01Vec * u + edge02Vec * v
        // in matrix form: 
        // [ rayStart - faceVertex_0 ] = [-rayVec  edge01Vec  edge02Vec ] [t u v]T

    // if we rearrange faceVertex_0 + edge01Vec * u + edge02Vec * v we get
    // 1 - (u + v) faceVertex_0 + faceVertex_1 * u + faceVertex_2 * v
    // or barycentric coords


    

    Homogeneous4 ret = Homogeneous4(0.f, 0.f, 0.f, -1.f);

    // check determinate of matrix [-rayVec edge01Vec edge02Vec] to make sure there is a solution
    float det = (rayVec * -1.).dot( edge01Vec.cross(edge02Vec) ); // this is equivalent to the determinant (found on wikipedia)
    if (det == 0.) return ret; // no intersection

    Cartesian3 rSfV = rayStart - faceVertex_0;

    float t = ( edge01Vec.cross(edge02Vec)      ).dot(rSfV) / det;
    float u = ( edge02Vec.cross(rayVec * -1.)   ).dot(rSfV) / det;
    float v = ( (rayVec * -1.).cross(edge01Vec) ).dot(rSfV) / det;

    // if we are out of our depth bounds, then immediately it counts as no collision
    if (!(t <= 1. && t >= 0.)) return ret; // return with w = -1

    // if these values don't satisfy the conditions described above, return with w of ret as -1 to signal no triangle collision
    if (!( u <= 1. && u >= 0. 
        && v <= 1. && v >= 0. 
        && u + v <= 1. )) {
            if (texturedObject->faceVertices[faceID].size() > 3 + vmod) { // if there are more vertices than we considered
                if (RayCollision(rayStart, rayEnd, faceID, vmod + 1).w == -1.f) // and if there is not a collision in the triangle defined by the next point (or, by recursion, any collisions in any triangles after that)
                    return ret;
                // if we find a w that is not -1, then we use the later return as we only want barycentric coordinates to be based on the first three vertices
                // this does assume that all points on a face are coplanar, but that should not be a stretch to assume
            } 
            // otherwise we've considered everything and found no collisions
            else return ret; // will be 0, 0, 0, -1
        }

    ret.x = 1.f - (u + v);
    ret.y = u;
    ret.z = v;
    ret.w = t;
    return ret;

}
    
// routine that generates the image
void RaytraceRenderWidget::Raytrace() { // RaytraceRenderWidget::Raytrace()

    // if paused, do nothing
    if (renderParameters->pauseRender) return;


    /// setup
    // always happens
    TransformVertices();
    // if lighting
    if (renderParameters->useLighting) {
        PrepareLights();
        PrepareMaterials();
    }

    // need to seed randoms for later
    srand(27); // 27 is my lucky number

    float hRayStep = (rightClip - leftClip) / frameBuffer.width;
    float vRayStep = (topClip - botClip) / frameBuffer.height;  
 

    Cartesian3 eyePoint = Cartesian3(0., 0., nearClip);
    // ortho ray dir
    Cartesian3 rayDir = Cartesian3(0., 0., -1.f);

    /// vars needed in every ray
    // collision info Homogeneous4
    Homogeneous4 collInfo;
    Homogeneous4 closestColl;
    // XYZ are actually barycentric coords, 
    // W is point along ray at which collision occurs for rayStart + rayVec * W

    // finding closest face vars
    float closestDepth = 1.1f;
    int nearFaceID;

    // ray casting per pixel
    for (int i = 0; i < frameBuffer.width; i ++)
        for (int j = 0; j < frameBuffer.height; j++) {
            /// shoot rays
            // calculate ray start of that particular point
            Cartesian3 rayStart = Cartesian3( leftClip + i * hRayStep, botClip + j * vRayStep, 0.) + eyePoint;
            // rayDir defaults to ortho
            Cartesian3 rayVec = rayDir * abs(farClip - nearClip);
            Cartesian3 rayEnd = rayStart + rayVec;

            // reset closestDepth to 1.1
            closestDepth = 1.1f;
            // any actual collision will have a depth value between 0 (near) and 1 (far)

            // check all faces for collision with ray
            for (int k = 0; k < texturedObject->faceVertices.size(); k ++) {
                collInfo = RayCollision(rayStart, rayEnd, k);
                if (collInfo.w > 0 && collInfo.w < closestDepth) {
                    closestDepth = collInfo.w;
                    closestColl = collInfo;
                    nearFaceID = k;
                }
            }

            
            if (closestDepth > 1.) {
                frameBuffer[j][i] = ClearColor;
                continue;
            }
            // if closestDepth is less than or equal to 1, a collision happened
            // this also requires closestDepth to be greater than 0, but we ensure any depth we write to closestDepth is greater than 0 already

            // from here on we decide what color to write to frameBuffer[j][i]

            // set to surface color as default
            RGBAValue pixelColor = RGBAValue(surfaceColor[0] * 255.f, surfaceColor[1] * 255.f, surfaceColor[2] * 255.f, 255.f);

            if (renderParameters->mapUVWToRGB || renderParameters->texturedRendering || renderParameters->useMoreMaterials) {

                // barycentric calculation of texture coord at that point
                Cartesian3 texCoord = texturedObject->textureCoords[texturedObject->faceTexCoords[nearFaceID][0]] * closestColl.x
                                    + texturedObject->textureCoords[texturedObject->faceTexCoords[nearFaceID][1]] * closestColl.y 
                                    + texturedObject->textureCoords[texturedObject->faceTexCoords[nearFaceID][2]] * closestColl.z;
                // tex coords should have UVW between 0 and 1, and barycentric coords total to 1, so everything should stay within that range

                if (renderParameters->showAxes)
                            texCoord = normals[texturedObject->faceNormals[nearFaceID][0]] * 0.5f + Cartesian3(0.5f, 0.5f, 0.5f) / 3.f
                                    + normals[texturedObject->faceNormals[nearFaceID][1]] * 0.5f + Cartesian3(0.5f, 0.5f, 0.5f) / 3.f 
                                    + normals[texturedObject->faceNormals[nearFaceID][2]] * 0.5f + Cartesian3(0.5f, 0.5f, 0.5f) / 3.f;

                if (renderParameters->mapUVWToRGB) {
                    pixelColor = RGBAValue(texCoord.x * 255.f, texCoord.y * 255.f, texCoord.z * 255.f, 255.f);

                    // in TexturedObject::Render, this is how mapUVWToRGB interacts with lighting
                    if (renderParameters->useLighting) { 
                        ambientColor[0] = diffuseColor[0] = specularColor[0] = texCoord.x;
                        ambientColor[1] = diffuseColor[1] = specularColor[1] = texCoord.y;
                        ambientColor[2] = diffuseColor[2] = specularColor[2] = texCoord.z;
                    }
                }
                
                if (renderParameters->useMoreMaterials) {
                    int matID = (int) texturedObject->textureCoords[texturedObject->faceTexCoords[nearFaceID][0]].z;
                    if (matID >= materials_ambients.size()) matID = 0;
                    for (int m = 0; m < 4; m ++) {
                        emissiveColor[m] = materials_emissive[matID][m];
                        ambientColor[m] = materials_ambients[matID][m];
                        diffuseColor[m] = materials_diffuse[matID][m];
                        specularColor[m] = materials_specular[matID][m];
                    }
                    
                    shininess = materials_shininess[matID];
                }
                
                if (renderParameters->texturedRendering) {
                    int texID = 0;
                    if (texturedObject->textures.size() > 1) texID = (int) texturedObject->textureCoords[texturedObject->faceTexCoords[nearFaceID][0]].z;
                    // use W of UVW to indicate texture as we have no 3D textures
                    if (texID >= texturedObject->textures.size()) texID = 0;
                    RGBAValue texValue = texturedObject->textures[texID][(int) (texCoord.y * texturedObject->textures[texID].height)][(int) (texCoord.x * texturedObject->textures[texID].width)];
                    if (renderParameters->textureModulation) pixelColor = pixelColor.modulate( texValue );
                    else {
                        pixelColor = texValue;
                        // [height][width]
                        frameBuffer[j][i] = pixelColor;
                        continue;
                        // won't be modulated by anything so just say we're done
                    }
                    
                }
            } // end tex coord if

            // LIGHTING
            if (renderParameters->useLighting) {
                float intensity[4] = {0.f, 0.f, 0.f, 0.f};

                // add emission to intensity
                intensity[0] += emissiveColor[0];
                intensity[1] += emissiveColor[1];
                intensity[2] += emissiveColor[2];
                intensity[3] += emissiveColor[3];

                // set up key vals
                Cartesian3 surfacePoint = rayStart + rayVec * closestColl.w;
                Cartesian3 toEye = rayDir.unit() * -1.f; // should already be unit but just to be safe
                Cartesian3 toLight;
                Cartesian3 normal;

                // check if we have smooth or flat normals based on if normals are 1 per vertex or not
                if (texturedObject->vertices.size() == texturedObject->normals.size())
                    normal  = normals[texturedObject->faceNormals[nearFaceID][0]] * closestColl.x
                            + normals[texturedObject->faceNormals[nearFaceID][1]] * closestColl.y 
                            + normals[texturedObject->faceNormals[nearFaceID][2]] * closestColl.z;
                else
                    normal  = normals[texturedObject->faceNormals[nearFaceID][0]];
                    //normal  = normals[texturedObject->faceNormals[nearFaceID][0]] / 3.f
                    //        + normals[texturedObject->faceNormals[nearFaceID][1]] / 3.f 
                    //        + normals[texturedObject->faceNormals[nearFaceID][2]] / 3.f;


                // just to be safe:
                normal = normal.unit();

                // move surface point tiny amount in the direction of normal?
                //surfacePoint = surfacePoint + normal * 0.1f;

                // per light
                for (int l = 0; l < lights.size(); l ++) {
                    // multiply by 2 * object radius so that we ensure that if used as the shadow checking direction we check entire span of the object for collisions
                    toLight = lights[l].pos.Vector().unit() * -1.f * (2.f * texturedObject->objectSize);
                    if (lights[l].pos.w != 0.f) toLight = (surfacePoint) - lights[l].pos.Point();

                    float attenuation = 1.0f;
                    if (renderParameters->useShadowRays) {
                        // check all faces for collision with shadow ray
                        for (int k = 0; k < texturedObject->faceVertices.size(); k ++) {
                            if (k == nearFaceID) continue; // if face to test is face the first collision is on, ignore checks
                            // offset starting point by small amount of normal so that we can check if own face collides with ray
                            //collInfo = RayCollision(surfacePoint + normal * 0.01f, surfacePoint + toLight, k);
                            collInfo = RayCollision(surfacePoint, surfacePoint + toLight, k);
                            if (collInfo.w >= 0 && collInfo.w <= 1.f) {
                                // if we find a collision, get outta there
                                attenuation = 0.f;
                                break;
                            }
                        }
                        // if we collided, attenuation will then be 0
                        if (attenuation == 0.f) {
                            // if we collided, then something is in the way, so only do ambient light from this light source
                            intensity[0] += lights[l].ambient[0] * ambientColor[0];
                            intensity[1] += lights[l].ambient[1] * ambientColor[1];
                            intensity[2] += lights[l].ambient[2] * ambientColor[2];
                            intensity[3] += lights[l].ambient[3] * ambientColor[3];
                            continue; // break so we don't do any more
                        }
                        // if w = 0, light is at infinity
                        // and attenuation will already be 1 (unless already dealt with)
                        if (lights[l].pos.w != 0.f) attenuation = toLight.dot(toLight); // this is the same as length of toLight squared
    
                    } // end if shadow rays


                    toLight = toLight.unit();
                    float lambertDiffuse = normal.dot(toLight) / attenuation;
                    if (lambertDiffuse < 0.f) lambertDiffuse = 0.f;
                    float glossSpecular = 0.f;
                    Cartesian3 avgEyeLight = (toEye + toLight) * 0.5f;
                    if (avgEyeLight.length() > 0.f) 
                        glossSpecular = pow(normal.dot(avgEyeLight.unit()), shininess) / attenuation;
                    if (glossSpecular < 0.f) glossSpecular = 0.f;

                    // direct light
                    intensity[0] += lights[l].diffuse[0] * diffuseColor[0] * lambertDiffuse;
                    intensity[1] += lights[l].diffuse[1] * diffuseColor[1] * lambertDiffuse;
                    intensity[2] += lights[l].diffuse[2] * diffuseColor[2] * lambertDiffuse;
                    intensity[3] += lights[l].diffuse[3] * diffuseColor[3] * 1.f;

                    intensity[0] += lights[l].specular[0] * specularColor[0] * glossSpecular;
                    intensity[1] += lights[l].specular[1] * specularColor[1] * glossSpecular;
                    intensity[2] += lights[l].specular[2] * specularColor[2] * glossSpecular;
                    intensity[3] += lights[l].specular[3] * specularColor[3] * 1.f;

                    // indirect light
                    intensity[0] += lights[l].ambient[0] * ambientColor[0];
                    intensity[1] += lights[l].ambient[1] * ambientColor[1];
                    intensity[2] += lights[l].ambient[2] * ambientColor[2];
                    intensity[3] += lights[l].ambient[3] * ambientColor[3];

                } // end loop through lights

                // scale from 0-1 to 0-255 
                RGBAValue lightColor = RGBAValue(intensity[0] * 255.f, intensity[1] * 255.f, intensity[2] * 255.f, intensity[3] * 255.f);
                if (renderParameters->textureModulation) pixelColor = pixelColor.modulate( lightColor );
                else pixelColor = lightColor;
            } // end if lighting 

            
            // [height][width]
            frameBuffer[j][i] = pixelColor;
            
                

        } // end ray loop

} // RaytraceRenderWidget::Raytrace()

//float RaytraceRenderWidget::BRDF(Cartesian3 normal, Cartesian3 inDir, Cartesian3 outDir) {

//}

Cartesian3 RaytraceRenderWidget::MonteCarloHemiVec(Cartesian3 norm) {
    Matrix4 transformationMatrix = Matrix4();
    transformationMatrix.SetIdentity();
    // angle between them is acos the dot product
    float theta = acos(Cartesian3(0., 1., 0.).dot(norm));
    Matrix4 rotationMatrix = Matrix4();
    rotationMatrix.SetRotation(Cartesian3(0., 1., 0.).cross(norm), theta);
    transformationMatrix = transformationMatrix * rotationMatrix;

    // get random u and v
    float u,v;
    u = ((float) (rand() % 1000))/1000.f;
    v = ((float) (rand() % 1000))/1000.f;

    Homogeneous4 randVec = Homogeneous4(cos(2 * M_PI * u), v, sin(2 * M_PI * u), 0.f);
    return (transformationMatrix * randVec).Vector().unit();
} // MonteCarloHemiVec()

void RaytraceRenderWidget::ClearBuffer() {
    for (int i = 0; i < frameBuffer.width; i ++)
        for (int j = 0; j < frameBuffer.height; j++)
            frameBuffer[j][i] = ClearColor;
}


    
// mouse-handling
void RaytraceRenderWidget::mousePressEvent(QMouseEvent *event)
    { // RaytraceRenderWidget::mousePressEvent()
    // store the button for future reference
    int whichButton = event->button();
    // scale the event to the nominal unit sphere in the widget:
    // find the minimum of height & width   
    float size = (width() > height()) ? height() : width();
    // scale both coordinates from that
    float x = (2.0 * event->x() - size) / size;
    float y = (size - 2.0 * event->y() ) / size;

    
    // and we want to force mouse buttons to allow shift-click to be the same as right-click
    int modifiers = event->modifiers();
    
    // shift-click (any) counts as right click
    if (modifiers & Qt::ShiftModifier)
        whichButton = Qt::RightButton;
    
    // send signal to the controller for detailed processing
    emit BeginScaledDrag(whichButton, x,y);
    } // RaytraceRenderWidget::mousePressEvent()
    
void RaytraceRenderWidget::mouseMoveEvent(QMouseEvent *event)
    { // RaytraceRenderWidget::mouseMoveEvent()
    // scale the event to the nominal unit sphere in the widget:
    // find the minimum of height & width   
    float size = (width() > height()) ? height() : width();
    // scale both coordinates from that
    float x = (2.0 * event->x() - size) / size;
    float y = (size - 2.0 * event->y() ) / size;
    
    // send signal to the controller for detailed processing
    emit ContinueScaledDrag(x,y);
    } // RaytraceRenderWidget::mouseMoveEvent()
    
void RaytraceRenderWidget::mouseReleaseEvent(QMouseEvent *event)
    { // RaytraceRenderWidget::mouseReleaseEvent()
    // scale the event to the nominal unit sphere in the widget:
    // find the minimum of height & width   
    float size = (width() > height()) ? height() : width();
    // scale both coordinates from that
    float x = (2.0 * event->x() - size) / size;
    float y = (size - 2.0 * event->y() ) / size;
    
    // send signal to the controller for detailed processing
    emit EndScaledDrag(x,y);
    } // RaytraceRenderWidget::mouseReleaseEvent()
