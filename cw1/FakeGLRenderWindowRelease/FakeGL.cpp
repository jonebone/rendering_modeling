//////////////////////////////////////////////////////////////////////
//
//  University of Leeds
//  COMP 5812M Foundations of Modelling & Rendering
//  User Interface for Coursework
//
//  September, 2020
//
//  ------------------------
//  FakeGL.cpp
//  ------------------------
//  
//  A unit for implementing OpenGL workalike calls
//  
///////////////////////////////////////////////////

#include "FakeGL.h"
#include <math.h>

//-------------------------------------------------//
//                                                 //
// CONSTRUCTOR / DESTRUCTOR                        //
//                                                 //
//-------------------------------------------------//

// constructor
FakeGL::FakeGL() { // constructor
    clearColor = RGBAValue(255.0f, 0.0f, 170.0f, 1.0f);
    frameBuffer = RGBAImage();
    modelviewMatrix = Matrix4();
    projectionMatrix = Matrix4();
    modelviewMatrixStack = std::deque<Matrix4>();
    projectionMatrixStack = std::deque<Matrix4>();
    currentColor = RGBAValue();
    fakeGLViewport;
    pointSize = 5.0;
    lineWidth = 5.0;
    depthTestEnabled = true;
    lightEnabled = false;
    textureEnabled = false;
    phongEnabled = false;
    vertexQueue = std::deque<vertexWithAttributes>();
    rasterQueue = std::deque<screenVertexWithAttributes>();
    fragmentQueue = std::deque<fragmentWithAttributes>();
} // constructor

// destructor
FakeGL::~FakeGL() { // destructor
    delete texture;
    //std::cout << (*this) << std::endl;
} // destructor

//-------------------------------------------------//
//                                                 //
// GEOMETRIC PRIMITIVE ROUTINES                    //
//                                                 //
//-------------------------------------------------//

// starts a sequence of geometric primitives
// types are :
/*
const unsigned int FAKEGL_POINTS = 0;
const unsigned int FAKEGL_LINES = 1;
const unsigned int FAKEGL_TRIANGLES = 2; // <= this is a "triangle soup" situation
*/
void FakeGL::Begin(unsigned int PrimitiveType) { // Begin()
    currentPrimitiveType = PrimitiveType;
} // Begin()

// ends a sequence of geometric primitives
void FakeGL::End() { // End()
    // if anything remains in the queues, flush it to screen
    Flush();
} // End()

// sets the size of a point for drawing
void FakeGL::PointSize(float size) { // PointSize()
    pointSize = size;
} // PointSize()

// sets the width of a line for drawing purposes
void FakeGL::LineWidth(float width) { // LineWidth()
    lineWidth = width;
} // LineWidth()

//-------------------------------------------------//
//                                                 //
// MATRIX MANIPULATION ROUTINES                    //
//                                                 //
//-------------------------------------------------//

// set the matrix mode (i.e. which one we change)   
// two modes: 
/*
const unsigned int FAKEGL_MODELVIEW = 1;
const unsigned int FAKEGL_PROJECTION = 2;
*/
// need two stacks - one for each
void FakeGL::MatrixMode(unsigned int whichMatrix) { // MatrixMode()
    if (whichMatrix == 1) {
        currentMatrix = &modelviewMatrix;
        currentMatrixStack = &modelviewMatrixStack;
    }
    else if (whichMatrix == 2) {
        currentMatrix = &projectionMatrix;
        currentMatrixStack = &projectionMatrixStack;
    }    
} // MatrixMode()

// pushes a matrix on the stack
void FakeGL::PushMatrix() { // PushMatrix()
    currentMatrixStack->push_back(Matrix4(currentMatrix[0]));
} // PushMatrix()

// pops a matrix off the stack
void FakeGL::PopMatrix() { // PopMatrix()
    /// transform vertex uses the current modelview matrix and the current projection matrix 
    //so if one of these would change, then all the vertices relying on it should be processed.
    while (!vertexQueue.empty())
        TransformVertex();
    currentMatrix[0] = currentMatrixStack->back();
    currentMatrixStack->pop_back();
} // PopMatrix()

// load the identity matrix
void FakeGL::LoadIdentity() { // LoadIdentity()
    currentMatrix->SetIdentity();
} // LoadIdentity()

// multiply by a known matrix in column-major format
void FakeGL::MultMatrixf(const float *columnMajorCoordinates) { // MultMatrixf()
    Matrix4 inputMatrix = Matrix4();
    for (int col = 0; col < 4; col++)
        for (int row = 0; row < 4; row++)
            inputMatrix[row][col] = columnMajorCoordinates[row + 4 * col];
    currentMatrix[0] = currentMatrix[0] * inputMatrix;
} // MultMatrixf()

// sets up a perspective projection matrix
// multiplies top of stack (current) matrix by frustum matrix
void FakeGL::Frustum(float left, float right, float bottom, float top, float zNear, float zFar) { // Frustum()
    Matrix4 frustumMatrix = Matrix4();
    frustumMatrix.SetZero();
    frustumMatrix[0][0] = 2.0f * zNear / (right - left);
    frustumMatrix[0][2] = (right + left) / (right - left);
    frustumMatrix[1][1] = 2.0f * zNear / (top - bottom);
    frustumMatrix[1][2] = (top + bottom) / (top - bottom);
    frustumMatrix[2][2] = -1.0f * (zFar + zNear) / (zFar - zNear);
    frustumMatrix[2][3] = -2.0f * zFar * zNear / (zFar - zNear);
    currentMatrix[0] = currentMatrix[0] * frustumMatrix;
} // Frustum()

// sets an orthographic projection matrix
// multiplies top of stack (current) matrix by ortho matrix
void FakeGL::Ortho(float left, float right, float bottom, float top, float zNear, float zFar) { // Ortho()
    Matrix4 orthoMatrix = Matrix4();
    orthoMatrix.SetZero();
    orthoMatrix[0][0] = 2.0f/(right - left);
    orthoMatrix[0][3] = -1.0f * (right + left)/(right - left);
    orthoMatrix[1][1] = 2.0f/(top - bottom);
    orthoMatrix[1][3] = -1.0f * (top + bottom)/(top - bottom);
    orthoMatrix[2][2] = -2.0f/(zFar - zNear);
    orthoMatrix[2][3] = -1.0f * (zFar + zNear)/(zFar - zNear);
    orthoMatrix[3][3] = 1.0f;
    currentMatrix[0] = currentMatrix[0] * orthoMatrix;
} // Ortho()

// rotate the matrix
void FakeGL::Rotatef(float angle, float axisX, float axisY, float axisZ) { // Rotatef()
    // rotatef should take degrees 
    // but SetRotation takes radians
    Matrix4 rotationMatrix = Matrix4();
    rotationMatrix.SetRotation(Cartesian3(axisX, axisY, axisZ), angle * M_PI / 180.0f);
    currentMatrix[0] = currentMatrix[0] * rotationMatrix;
} // Rotatef()

// scale the matrix
void FakeGL::Scalef(float xScale, float yScale, float zScale) { // Scalef()
    Matrix4 scaleMatrix = Matrix4();
    scaleMatrix.SetScale(xScale, yScale, zScale);
    currentMatrix[0] = currentMatrix[0] * scaleMatrix;
} // Scalef()

// translate the matrix
void FakeGL::Translatef(float xTranslate, float yTranslate, float zTranslate) { // Translatef()
    Matrix4 translationMatrix = Matrix4();
    translationMatrix.SetTranslation(Cartesian3(xTranslate, yTranslate, zTranslate));
    currentMatrix[0] = currentMatrix[0] * translationMatrix;
} // Translatef()

// sets the viewport
// need a viewport to set
void FakeGL::Viewport(int x, int y, int width, int height){ // Viewport()
    fakeGLViewport[0] = x;
    fakeGLViewport[1] = y;
    fakeGLViewport[2] = width;
    fakeGLViewport[3] = height;
    depthBuffer.Resize(width, height);
} // Viewport()

//-------------------------------------------------//
//                                                 //
// VERTEX ATTRIBUTE ROUTINES                       //
//                                                 //
//-------------------------------------------------//

// vertexWithAttributes has a color attribute
// sets color with floating point
void FakeGL::Color3f(float red, float green, float blue) { // Color3f()
    currentColor = RGBAValue(255.0f * red, 255.0f * green, 255.0f * blue, 255.0f);
} // Color3f()

// constants for Light() - actually bit flags
//const unsigned int FAKEGL_POSITION = 1;
//const unsigned int FAKEGL_AMBIENT = 2;
//const unsigned int FAKEGL_DIFFUSE = 4;
//const unsigned int FAKEGL_AMBIENT_AND_DIFFUSE = 6;
//const unsigned int FAKEGL_SPECULAR = 8;
// additional constants for Material()
//const unsigned int FAKEGL_EMISSION = 16;  // set emissive color
//const unsigned int FAKEGL_SHININESS = 32; // set specular exponent

// vertexWithAttributes may need material attributes
// sets material properties
void FakeGL::Materialf(unsigned int parameterName, const float parameterValue) { // Materialf()
    if (parameterName & FAKEGL_SHININESS) m_shininess = parameterValue; 
} // Materialf()

void FakeGL::Materialfv(unsigned int parameterName, const float *parameterValues) { // Materialfv()
    if (parameterName & FAKEGL_AMBIENT) {
        m_ambientColor = RGBAValue(255.0f * parameterValues[0], 255.0f * parameterValues[1], 255.0f * parameterValues[2], 255.0f * parameterValues[3]);
    }
    if (parameterName & FAKEGL_DIFFUSE) {
        m_diffuseColor = RGBAValue(255.0f * parameterValues[0], 255.0f * parameterValues[1], 255.0f * parameterValues[2], 255.0f * parameterValues[3]);
    }
    if (parameterName & FAKEGL_SPECULAR) {
        m_specularColor = RGBAValue(255.0f * parameterValues[0], 255.0f * parameterValues[1], 255.0f * parameterValues[2], 255.0f * parameterValues[3]);
    }
    if (parameterName & FAKEGL_EMISSION) {
        m_emissiveColor = RGBAValue(255.0f * parameterValues[0], 255.0f * parameterValues[1], 255.0f * parameterValues[2], 255.0f * parameterValues[3]);
    }
    //std::cout << "ambient material: \n" << m_ambientColor << "\ndiffuse material: \n" << m_diffuseColor << std::endl;
} // Materialfv()

// vertexWithAttributes may need normal attribute
// sets the normal vector
void FakeGL::Normal3f(float x, float y, float z) { // Normal3f()
    lastNormal = Homogeneous4(x, y, z, 0.0);
} // Normal3f()


// sets the texture coordinates
void FakeGL::TexCoord2f(float u, float v) { // TexCoord2f()
    lastTexCoord[0] = u;
    lastTexCoord[1] = v;
} // TexCoord2f()

// sets the vertex & launches it down the pipeline
void FakeGL::Vertex3f(float x, float y, float z) { // Vertex3f()
    Homogeneous4 pos = Homogeneous4(x, y, z, 1.0f);
    vertexWithAttributes* v = new vertexWithAttributes();
    v->position = pos;
    v->color = currentColor;
    v->primitiveType = currentPrimitiveType;
    // set normal
    v->normal = lastNormal;
    /* Assuming all materials are identical to vertex color, we can just use vertex color instead of materials 
    // set colors for lighting
    v->ambientColor = m_ambientColor;
    v->diffuseColor = m_diffuseColor;
    v->specularColor = m_specularColor;
    v->emissiveColor = m_emissiveColor;
    v->shininess = m_shininess;
    */
    v->texCoord[0] = lastTexCoord[0];
    v->texCoord[1] = lastTexCoord[1];
    
    vertexQueue.push_back(v[0]);

    // sending vertices down the pipeline
    TransformVertex();
    RasterisePrimitive();
    ProcessFragment();
} // Vertex3f()

//-------------------------------------------------//
//                                                 //
// STATE VARIABLE ROUTINES                         //
//                                                 //
//-------------------------------------------------//

//// constants for Enable()/Disable()
//const unsigned int FAKEGL_LIGHTING = 1;
//const unsigned int FAKEGL_TEXTURE_2D = 2;
//const unsigned int FAKEGL_DEPTH_TEST = 3;
//const unsigned int FAKEGL_PHONG_SHADING = 4;

// disables a specific flag in the library
void FakeGL::Disable(unsigned int property) { // Disable()
    if      (property == FAKEGL_LIGHTING)   lightEnabled = false;
    else if (property == FAKEGL_TEXTURE_2D) textureEnabled = false;
    else if (property == FAKEGL_DEPTH_TEST) depthTestEnabled = false;
    else if (property == FAKEGL_PHONG_SHADING) phongEnabled = false;

} // Disable()

// enables a specific flag in the library
void FakeGL::Enable(unsigned int property) { // Enable()
    if      (property == FAKEGL_LIGHTING)   lightEnabled = true;
    else if (property == FAKEGL_TEXTURE_2D) textureEnabled = true;
    else if (property == FAKEGL_DEPTH_TEST) depthTestEnabled = true;
    else if (property == FAKEGL_PHONG_SHADING) phongEnabled = true;
} // Enable()

//-------------------------------------------------//
//                                                 //
// LIGHTING STATE ROUTINES                         //
//                                                 //
//-------------------------------------------------//

/// constants for Light() - actually bit flags
//const unsigned int FAKEGL_POSITION = 1;               Homogeneous4
//const unsigned int FAKEGL_AMBIENT = 2;                RGBAValue
//const unsigned int FAKEGL_DIFFUSE = 4;                RGBAValue
//const unsigned int FAKEGL_AMBIENT_AND_DIFFUSE = 6;    RGBAValue
//const unsigned int FAKEGL_SPECULAR = 8;               RGBAValue

// sets properties for the one and only light
void FakeGL::Light(int parameterName, const float *parameterValues) { // Light()
    if (parameterName & FAKEGL_POSITION) {
        lightPosition = Homogeneous4(parameterValues[0], parameterValues[1], parameterValues[2], parameterValues[3]);
        //light is not used again until transform time so transform it by modelview now
        lightPosition = modelviewMatrix * lightPosition;
        // transform it by projection matrix as needed
    }
    if (parameterName & FAKEGL_AMBIENT) {
        ambientColor = RGBAValue(255.0f * parameterValues[0], 255.0f * parameterValues[1], 255.0f * parameterValues[2], 255.0f * parameterValues[3]);
        //ambientColor = RGBAValue((float)pow(255.0f, parameterValues[0]), (float)pow(255.0f, parameterValues[1]), (float)pow(255.0f, parameterValues[2]), (float)pow(255.0f, parameterValues[3]));
    }
    if (parameterName & FAKEGL_DIFFUSE) {
        diffuseColor = RGBAValue(255.0f * parameterValues[0], 255.0f * parameterValues[1], 255.0f * parameterValues[2], 255.0f * parameterValues[3]);
    }
    if (parameterName & FAKEGL_SPECULAR) {
        specularColor = RGBAValue(255.0f * parameterValues[0], 255.0f * parameterValues[1], 255.0f * parameterValues[2], 255.0f * parameterValues[3]);
    }
} // Light()

//-------------------------------------------------//
//                                                 //
// TEXTURE PROCESSING ROUTINES                     //
//                                                 //
// Note that we only allow one texture             //
// so glGenTexture & glBindTexture aren't needed   //
//                                                 //
//-------------------------------------------------//

/// constants for texture operations
//const unsigned int FAKEGL_MODULATE = 1;
//const unsigned int FAKEGL_REPLACE = 2;
/// sets whether textures replace or modulate
void FakeGL::TexEnvMode(unsigned int textureModeFlag){ // TexEnvMode()
    if (textureModeFlag == FAKEGL_MODULATE || textureModeFlag == FAKEGL_REPLACE)
        textureMode = textureModeFlag;
} // TexEnvMode()

// sets the texture image that corresponds to a given ID
void FakeGL::TexImage2D(const RGBAImage &textureImage){ // TexImage2D()
    //set own texture to that of the passed value
    // reconstruct texture to avoid potential pointer issues
    texture = new RGBAImage(textureImage);
} // TexImage2D()

//-------------------------------------------------//
//                                                 //
// FRAME BUFFER ROUTINES                           //
//                                                 //
//-------------------------------------------------//

/// bitflag constants for Clear()
//const unsigned int FAKEGL_COLOR_BUFFER_BIT = 1;
//const unsigned int FAKEGL_DEPTH_BUFFER_BIT = 2;

// clears the frame buffer
void FakeGL::Clear(unsigned int mask) { // Clear()
    // clear queues bc if this was a parallel application clear could be enacted after putting things in queues but before the queues got to the screen
    //      which could result in things put into the pipeline before clear being drawn to screen after clearing the screen
    //      clearing the queues first eliminates this possibility
    vertexQueue.clear();
    rasterQueue.clear();
    fragmentQueue.clear();
    // clearing buffers themselves
    if (mask & FAKEGL_COLOR_BUFFER_BIT)
        for (int row = 0; row < frameBuffer.height; row ++)
            for (int col = 0; col < frameBuffer.width; col ++)
                frameBuffer[row][col] = clearColor;
    if (mask & FAKEGL_DEPTH_BUFFER_BIT)
        for (int row = 0; row < depthBuffer.height; row ++)
            for (int col = 0; col < depthBuffer.width; col ++)
                depthBuffer[row][col] = RGBAValue(); // RGBAValue defaults to being 0 red 0 green 0 blue
    

    // write clearColor pixels to framebuffer
    
} // Clear()

// sets the clear color for the frame buffer
void FakeGL::ClearColor(float red, float green, float blue, float alpha) { // ClearColor()
    clearColor.red = 255.0f * red;
    clearColor.blue = 255.0f * blue;
    clearColor.green = 255.0f * green;
    clearColor.alpha = 255.0f * alpha;
} // ClearColor()

//-------------------------------------------------//
//                                                 //
// ROUTINE TO FLUSH THE PIPELINE                   //
//                                                 //
//-------------------------------------------------//

// flushes the pipeline
void FakeGL::Flush(){
    while(!vertexQueue.empty()) TransformVertex();
    while(!rasterQueue.empty()) RasterisePrimitive();
    while(!fragmentQueue.empty()) ProcessFragment();
};

//-------------------------------------------------//
//                                                 //
// MAJOR PROCESSING ROUTINES                       //
//                                                 //
//-------------------------------------------------//

// transform one vertex & shift to the raster queue
void FakeGL::TransformVertex() { // TransformVertex()
    if (vertexQueue.empty()) return;
    screenVertexWithAttributes* sva = new screenVertexWithAttributes();
    // transform vertex position
    Homogeneous4 pos = vertexQueue.front().position;
    pos = modelviewMatrix * pos;
    pos = projectionMatrix * pos; 
    Cartesian3 cartPos = pos.Point(); // cartPos is now in DCS
    // coords within view box (either ortho box or frustum box) are now on range -1 to 1 for x, y, and z
    // convert x,y to window coordinates- (x = -1, y = -1) => (column Viewport x offset, row Viewport y offset)
    //                                    (x =  1, y =  1) => (column Viewport width   , row Viewport height  )
    //
    cartPos.x = cartPos.x * fakeGLViewport[2]/2 + fakeGLViewport[2]/2 + fakeGLViewport[0];
    cartPos.y = cartPos.y * fakeGLViewport[3]/2 + fakeGLViewport[3]/2 + fakeGLViewport[1];
    // set z to depth
    // -1 is near, 1 is far, *-1 to invert, + 1 to bring to 0-2 range, /2 to bring to 0-1 range, *255 to bring to 0-255 range where 0 is far 255 is near
    // in 255 scale bc cast to unsigned char to compare it against depthBuffer red color value
    cartPos.z = (-1 * cartPos.z + 1)/2 * 255;
    //
    //
    
    
    sva->position = cartPos;
    sva->primitiveType = vertexQueue.front().primitiveType;

    // give screen vector color
    RGBAValue passedColor = vertexQueue.front().color;

    /// lighting stuff 
    
    if (lightEnabled) {
        // transform normal and ensure it stays a unit vector
        // this is needed in phong shading and in gouraud shading
        Homogeneous4 normal = vertexQueue.front().normal;
        normal = projectionMatrix * (modelviewMatrix * normal);
        Cartesian3 normalDirection = normal.Vector().unit();

        if (phongEnabled){
            /* None of this matters if we just assume that vertex material properties are all the same as vertex color

            /// for phong shading, light values will be computed per fragment
            //      so separate properties must be given to instances of screenVertexWithAttributes for later use
            //      however, for diffuse and specular intensity
            //          I_component = l_component * r_component * C(normal)
            //      where l_component and r_component are the corresponding light and reflective material properties, respectively
            //      and C(normal) is some function of the normal
            //      for phong shading, we compute the C terms per fragment once we've interpolated normals across fragments
            //      but we can do the other parts here
            sva->diffuseColor = vertexQueue.front().diffuseColor.modulate(diffuseColor);
            sva->specularColor = vertexQueue.front().specularColor.modulate(specularColor);
            sva->shininess = vertexQueue.front().shininess;
            /// store l_ambient * r_ambient + emissive as the default color
            //  since they don't rely on normals for calculation
            //  plus this saves screenVertexWithAttributes from needing to store separate ambient and emissive colors
            passedColor = vertexQueue.front().ambientColor.modulate(ambientColor) + vertexQueue.front().emissiveColor;
            */
            /* except for this */
            // pass normal for later interpolation
            sva->normalVector = normalDirection;
        }
        else {
            // don't do the gourand shading stuff unless only light and not phong shading is enabled
            // transform light direction and normalize
            Cartesian3 lightDirection = (projectionMatrix * lightPosition).Vector().unit();
            // we don't need to divide normal dot lightDirection by the product of their lengths bc they are both unit vectors
            float diffuseCoefficient = normalDirection.dot(lightDirection);
            /// calculate specular highlights
            // since everything has been transformed to DCS, the eye vector from any point is [0, 0, -1]
            Cartesian3 eyeVector = Cartesian3(0.0f, 0.0f, -1.0f);
            Cartesian3 bVector = (eyeVector + lightDirection) / 2.0f; // the vector labelled V subscript b in the slides
            float specularCoefficient = pow( normalDirection.dot(bVector), m_shininess); // using state variable value for shininess

            /// condense light components into single color 
            // do this here since gouraud shading is per vertex, so we do shading of the vertices here and interpolate color over fragments later
            // could pass attributes to screen vertex and calculate there, but then might as well do phong
            /* not using materials bc we assume that since vertex material properties are all the same as the color attribute
            RGBAValue combinedColor = specularCoefficient * vertexQueue.front().specularColor.modulate(specularColor)
                                    + diffuseCoefficient * vertexQueue.front().diffuseColor.modulate(diffuseColor)
                                    + vertexQueue.front().ambientColor.modulate(ambientColor)
                                    + vertexQueue.front().emissiveColor;
            */
            // combined light components of all but emissive with materials factored out
            // since all materials are the same and are vertex color, l_specular*r_specular*C_specular + l_diffuse*r_diffuse*C_diffuse + l_ambient*r_ambient =
            // r(l_specular*C_specular + l_diffuse*C_diffuse + l_ambient)
            RGBAValue combinedColor = specularCoefficient * specularColor
                                    + diffuseCoefficient * diffuseColor
                                    + ambientColor;
            // also adding state value for emissive at the end
            passedColor = vertexQueue.front().color.modulate(combinedColor) + m_emissiveColor;
        }
    }

    // give texture coords
    sva->texCoord[0] = vertexQueue.front().texCoord[0];
    sva->texCoord[1] = vertexQueue.front().texCoord[1];
    // give final color value
    sva->color = passedColor;

    // in with the new out with the old
    rasterQueue.push_back(sva[0]);
    vertexQueue.pop_front();
} // TransformVertex()

// rasterise a single primitive if there are enough vertices on the queue
bool FakeGL::RasterisePrimitive() { // RasterisePrimitive()
    if (rasterQueue.empty()) return false;

    switch(rasterQueue.front().primitiveType){
        case FAKEGL_POINTS: {
            RasterisePoint(rasterQueue.front());
            //rasterQueue.pop_front();
            break;
        }
        case FAKEGL_LINES: {
            if (rasterQueue.size() >= 2)
                if (rasterQueue[1].primitiveType == FAKEGL_LINES)
                    RasteriseLineSegment(rasterQueue[0], rasterQueue[1]);
            break;
        }
        case FAKEGL_TRIANGLES: {
            if (rasterQueue.size() >= 3)
                if (rasterQueue[1].primitiveType == FAKEGL_TRIANGLES && rasterQueue[2].primitiveType == FAKEGL_TRIANGLES){
                    RasteriseTriangle(rasterQueue[0], rasterQueue[1], rasterQueue[2]);
                    // pop 3 since the first 3 screenVertexWithAttributes will be used by RasterizeTriangle
                    rasterQueue.pop_front();
                    rasterQueue.pop_front();
                    rasterQueue.pop_front();
                }
            break;
        }
        default:
            break;
    }
} // RasterisePrimitive()

// rasterises a single point
void FakeGL::RasterisePoint(screenVertexWithAttributes &vertex0) { // RasterisePoint()
    fragmentWithAttributes* rasterFragment;
    // use pointsize/2 so that radius is pointsize 
    for (int i = 0 - pointSize/2; i < pointSize/2; i ++) {
        for (int j = 0 - pointSize/2; j < pointSize/2; j++) {
            rasterFragment = new fragmentWithAttributes();
            rasterFragment->col = vertex0.position.x + i;
            rasterFragment->row = vertex0.position.y + j;
            rasterFragment->color = vertex0.color;
            rasterFragment->depth = vertex0.position.z;
            // single point doesn't need tex coords
            if (vertex0.position.z >= 0 && vertex0.position.z <= 255)
                fragmentQueue.push_back(rasterFragment[0]);
        }
    }

    rasterQueue.pop_front();
} // RasterisePoint()

// rasterises a single line segment
void FakeGL::RasteriseLineSegment(screenVertexWithAttributes &vertex0, screenVertexWithAttributes &vertex1) { // RasteriseLineSegment()
    //std::cout << "line: \nvertex 0\n" << vertex0 << "vertex 1\n" << vertex1 << std::endl;
    fragmentWithAttributes* rasterFragment;
    for (float t = 0.0f; t < 1.0f; t += 0.001) {
        // use linewidth/2 so that each step produces a dot w radius linewidth 
        for (int i = 0 - lineWidth/2; i < lineWidth/2; i ++) {
            for (int j = 0 - lineWidth/2; j < lineWidth/2; j++) {
                rasterFragment = new fragmentWithAttributes();
                rasterFragment->col = (vertex0.position.x * t) + (vertex1.position.x * (1.0f - t)) + i;
                rasterFragment->row = (vertex0.position.y * t) + (vertex1.position.y * (1.0f - t)) + j;
                rasterFragment->color = (t * vertex0.color ) + ((1.0f - t) * vertex1.color);
                // don't need texture for lines
                // need to have a depth float to check bounds because casting to char can make wrapping depths
                float depth = (vertex0.position.z * t) + (vertex1.position.z * (1.0f - t));
                rasterFragment->depth = depth;
                // check bounding
                if (rasterFragment->row < frameBuffer.height && rasterFragment->row >= 0 && rasterFragment->col < frameBuffer.width && rasterFragment->col >= 0)
                    if (depth >= 0 && depth <= 255)
                        fragmentQueue.push_back(rasterFragment[0]);
            }
        }
    }
    // pop twice since the first 2 screenVertexWithAttributes have been used
    rasterQueue.pop_front();
    rasterQueue.pop_front();
} // RasteriseLineSegment()

// rasterises a single triangle
void FakeGL::RasteriseTriangle(screenVertexWithAttributes &vertex0, screenVertexWithAttributes &vertex1, screenVertexWithAttributes &vertex2) { // RasteriseTriangle()
    //std::cout << "triangle: \nvertex 0\n" << vertex0 << "vertex 1\n" << vertex1 << "vertex 2\n" << vertex2 << std::endl;
    //std::cout << vertex0.diffuseCoefficient << " " << vertex1.diffuseCoefficient << " " << vertex2.diffuseCoefficient << std::endl;
    
    // compute a bounding box that starts inverted to frame size
    // clipping will happen in the raster loop proper
    float minX = frameBuffer.width, maxX = 0.0;
    float minY = frameBuffer.height, maxY = 0.0;
    
    // test against all vertices
    if (vertex0.position.x < minX) minX = vertex0.position.x;
    if (vertex0.position.x > maxX) maxX = vertex0.position.x;
    if (vertex0.position.y < minY) minY = vertex0.position.y;
    if (vertex0.position.y > maxY) maxY = vertex0.position.y;
    
    if (vertex1.position.x < minX) minX = vertex1.position.x;
    if (vertex1.position.x > maxX) maxX = vertex1.position.x;
    if (vertex1.position.y < minY) minY = vertex1.position.y;
    if (vertex1.position.y > maxY) maxY = vertex1.position.y;
    
    if (vertex2.position.x < minX) minX = vertex2.position.x;
    if (vertex2.position.x > maxX) maxX = vertex2.position.x;
    if (vertex2.position.y < minY) minY = vertex2.position.y;
    if (vertex2.position.y > maxY) maxY = vertex2.position.y;

    // now for each side of the triangle, compute the line vectors
    Cartesian3 vector01 = vertex1.position - vertex0.position;
    Cartesian3 vector12 = vertex2.position - vertex1.position;
    Cartesian3 vector20 = vertex0.position - vertex2.position;

    // now compute the line normal vectors
    Cartesian3 normal01(-vector01.y, vector01.x, 0.0);  
    Cartesian3 normal12(-vector12.y, vector12.x, 0.0);  
    Cartesian3 normal20(-vector20.y, vector20.x, 0.0);  

    // we don't need to normalise them, because the square roots will cancel out in the barycentric coordinates
    float lineConstant01 = normal01.dot(vertex0.position);
    float lineConstant12 = normal12.dot(vertex1.position);
    float lineConstant20 = normal20.dot(vertex2.position);

    // and compute the distance of each vertex from the opposing side
    float distance0 = normal12.dot(vertex0.position) - lineConstant12;
    float distance1 = normal20.dot(vertex1.position) - lineConstant20;
    float distance2 = normal01.dot(vertex2.position) - lineConstant01;
    
    // if any of these are zero, we will have a divide by zero error
    // but notice that if they are zero, the vertices are collinear in projection and the triangle is edge on
    // we can render that as a line, but the better solution is to render nothing.  In a surface, the adjacent
    // triangles will eventually take care of it
    if ((distance0 == 0) || (distance1 == 0) || (distance2 == 0))
        return; 

    
    // create a fragment for reuse
    fragmentWithAttributes rasterFragment;

    /*
    // transform light by projection matrix here so that the calculations aren't done in the loop
    // declaration must be outside the scope of the if
    Cartesian3 lightDirection, bVector;
    if (phongEnabled) {
        lightDirection = (projectionMatrix * lightPosition).Vector().unit();
        // since everything has been transformed into DCS, the eye vector from any point is (0, 0, -1)
        bVector = (Cartesian3(0.0f, 0.0f, -1.0f) + lightDirection) / 2.0f;
    }
    */

    // loop through the pixels in the bounding box
    for (rasterFragment.row = minY; rasterFragment.row <= maxY; rasterFragment.row++)
        { // per row
        // this is here so that clipping works correctly
        if (rasterFragment.row < 0) continue;
        if (rasterFragment.row >= frameBuffer.height) continue;
        for (rasterFragment.col = minX; rasterFragment.col <= maxX; rasterFragment.col++)
            { // per pixel
            // this is also for correct clipping
            if (rasterFragment.col < 0) continue;
            if (rasterFragment.col >= frameBuffer.width) continue;
            
            // the pixel in cartesian format
            Cartesian3 pixel(rasterFragment.col, rasterFragment.row, 0.0);
            
            // right - we have a pixel inside the frame buffer AND the bounding box
            // note we *COULD* compute gamma = 1.0 - alpha - beta instead
            float alpha = (normal12.dot(pixel) - lineConstant12) / distance0;           
            float beta = (normal20.dot(pixel) - lineConstant20) / distance1;            
            float gamma = (normal01.dot(pixel) - lineConstant01) / distance2;           

            // now perform the half-plane test
            if ((alpha < 0.0) || (beta < 0.0) || (gamma < 0.0))
                continue;

            // compute colors
            RGBAValue outputColor = alpha * vertex0.color + beta * vertex1.color + gamma * vertex2.color;
            /// if we're doing phong shading, do normal interpolation
            if (phongEnabled) {
                Cartesian3 fragNormal = alpha * vertex0.normalVector + beta * vertex1.normalVector + gamma * vertex2.normalVector;
                fragNormal = fragNormal.unit();
                rasterFragment.normalVector = fragNormal;
            }

            rasterFragment.color = outputColor; 
            // compute texture coords
            rasterFragment.texCoord[0] = alpha * vertex0.texCoord[0] + beta * vertex1.texCoord[0] + gamma * vertex2.texCoord[0];
            rasterFragment.texCoord[1] = alpha * vertex0.texCoord[1] + beta * vertex1.texCoord[1] + gamma * vertex2.texCoord[1];


            // depth
            float depth = alpha * vertex0.position.z + beta * vertex1.position.z + gamma * vertex2.position.z;
            rasterFragment.depth = depth;

            // now we add it to the queue for fragment processing
            if (depth >= 0 && depth <= 255)
                fragmentQueue.push_back(rasterFragment);
            } // per pixel
        } // per row
    
    
} // RasteriseTriangle()

// process a single fragment
void FakeGL::ProcessFragment() { // ProcessFragment()
    if (fragmentQueue.empty()) return;

    int screenx, screeny;
    screenx = fragmentQueue.front().col;
    screeny = fragmentQueue.front().row;
    RGBAValue outputColor = fragmentQueue.front().color;

    if (phongEnabled && lightEnabled) {
        Cartesian3 lightDirection, bVector;
        lightDirection = (projectionMatrix * lightPosition).Vector().unit();
        // since everything has been transformed into DCS, the eye vector from any point is (0, 0, -1)
        bVector = (Cartesian3(0.0f, 0.0f, -1.0f) + lightDirection) / 2.0f;
        Cartesian3 fragNormal = fragmentQueue.front().normalVector;
        float diffuseCoefficient = fragNormal.dot(lightDirection);
        float specularCoefficient = pow(fragNormal.dot(bVector),  m_shininess);

        RGBAValue combinedColor = specularCoefficient * specularColor
                                + diffuseCoefficient * diffuseColor
                                + ambientColor;
        // also adding state value for emissive at the end
        outputColor = outputColor.modulate(combinedColor) + m_emissiveColor;
    }

    if (textureEnabled) {
        // could add interpolation here if i wanted
        int u = (texture->height - 1) * fragmentQueue.front().texCoord[1];
        int v = (texture->width  - 1) * fragmentQueue.front().texCoord[0];
        
        // texture starts with [0] because texture is not an RGBAImage but a pointer to a RGBAImage 
        // so texture[0] is the actual RGBAImage
        RGBAValue texColor = texture[0][u][v];
        
        if (textureMode == FAKEGL_MODULATE)
            outputColor = outputColor.modulate(texColor);
        else outputColor = texColor;
    }


    if (depthTestEnabled) {
        if (depthBuffer[screeny][screenx].red < fragmentQueue.front().depth) {
            depthBuffer[screeny][screenx].red = fragmentQueue.front().depth;
            frameBuffer[screeny][screenx] = outputColor;
        }
    } else {
        frameBuffer[screeny][screenx] = outputColor;
    }
    
    fragmentQueue.pop_front();
    

} // ProcessFragment()

// standard routine for dumping the entire FakeGL context (except for texture / image)
std::ostream &operator << (std::ostream &outStream, FakeGL &fakeGL)
    { // operator <<
    outStream << "=========================" << std::endl;
    outStream << "Dumping FakeGL Context   " << std::endl;
    outStream << "=========================" << std::endl;
    outStream << "Viewport: \n x:" << fakeGL.fakeGLViewport[0] << "\n y:" << fakeGL.fakeGLViewport[1] << "\n width: " << fakeGL.fakeGLViewport[2] << "\n height:" << fakeGL.fakeGLViewport[3] << std::endl;
    outStream << "Lighting: \n ambient color:  " << fakeGL.ambientColor << "\n diffuse color:  " << fakeGL.diffuseColor << "\n specular color: " << fakeGL.specularColor << std::endl;

    outStream << "-------------------------" << std::endl;
    outStream << "modelview matrix stack:  " << std::endl;
    outStream << "-------------------------" << std::endl;
    for (auto matrix = fakeGL.modelviewMatrixStack.begin(); matrix < fakeGL.modelviewMatrixStack.end(); matrix++) { // per matrix
        outStream << "Matrix " << matrix - fakeGL.modelviewMatrixStack.begin() << std::endl;
        outStream << *matrix;
    } // per matrix

    outStream << "-------------------------" << std::endl;
    outStream << "projection matrix stack: " << std::endl;
    outStream << "-------------------------" << std::endl;
    for (auto matrix = fakeGL.projectionMatrixStack.begin(); matrix < fakeGL.projectionMatrixStack.end(); matrix++) { // per matrix
        outStream << "Matrix " << matrix - fakeGL.projectionMatrixStack.begin() << std::endl;
        outStream << *matrix;
    } // per matrix


    outStream << "-------------------------" << std::endl;
    outStream << "Vertex Queue:            " << std::endl;
    outStream << "-------------------------" << std::endl;
    for (auto vertex = fakeGL.vertexQueue.begin(); vertex < fakeGL.vertexQueue.end(); vertex++)
        { // per matrix
        outStream << "Vertex " << vertex - fakeGL.vertexQueue.begin() << std::endl;
        outStream << *vertex;
        } // per matrix


    outStream << "-------------------------" << std::endl;
    outStream << "Raster Queue:            " << std::endl;
    outStream << "-------------------------" << std::endl;
    for (auto vertex = fakeGL.rasterQueue.begin(); vertex < fakeGL.rasterQueue.end(); vertex++)
        { // per matrix
        outStream << "Vertex " << vertex - fakeGL.rasterQueue.begin() << std::endl;
        outStream << *vertex;
        } // per matrix

    for (auto fragment = fakeGL.fragmentQueue.begin(); fragment < fakeGL.fragmentQueue.end(); fragment++)
        { // per matrix
        outStream << "Fragment " << fragment - fakeGL.fragmentQueue.begin() << std::endl;
        outStream << *fragment;
        } // per matrix


    return outStream;
    } // operator <<

// subroutines for other classes
std::ostream &operator << (std::ostream &outStream, vertexWithAttributes &vertex)
    { // operator <<
    std::cout << "Vertex With Attributes" << std::endl;
    std::cout << "Position:   " << vertex.position << std::endl;
    std::cout << "Colour:     " << vertex.color << std::endl;

	// you

    return outStream;
    } // operator <<

std::ostream &operator << (std::ostream &outStream, screenVertexWithAttributes &vertex) 
    { // operator <<
    std::cout << "Screen Vertex With Attributes" << std::endl;
    std::cout << "Position:   " << vertex.position << std::endl;
    std::cout << "Colour:     " << vertex.color << std::endl;

    return outStream;
    } // operator <<

std::ostream &operator << (std::ostream &outStream, fragmentWithAttributes &fragment)
    { // operator <<
    std::cout << "Fragment With Attributes" << std::endl;
    std::cout << "Row:        " << fragment.row << std::endl;
    std::cout << "Col:        " << fragment.col << std::endl;
    std::cout << "Colour:     " << fragment.color << std::endl;

    return outStream;
    } // operator <<


    
    